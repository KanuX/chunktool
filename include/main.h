#ifndef MAIN_HEADER
#define MAIN_HEADER

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

/* typedefs for short variable declaration */
typedef signed char i8;
typedef short i16;
typedef int i32;
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;

/* general structures */
typedef struct ObjDefinition
{
  double pos;
  u8 negative;
} ObjDefinition;
typedef struct Position
{
  ObjDefinition x;
  ObjDefinition y;
  ObjDefinition z;
} Position;
typedef struct ChunkDefinition
{
  i32 x;
  i32 z;
} ChunkDefinition;
typedef struct Chunk{
  ChunkDefinition start;
  ChunkDefinition final;
  u8 size;
} Chunk;

#endif
