CC = gcc
CFLAGS = --std=c89 -g -Wall
INCLUDES = -I include/
LIBRARIES =
SOURCES = src/main.c \
		  src/calculator.c

all:
	${CC} ${CFLAGS} ${SOURCES} ${INCLUDES} ${LIBRARIES} -o chunktool
clean:
	@rm -v chunktool || true
