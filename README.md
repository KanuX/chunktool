ChunkTool
=========

A chunk calculator.

Usage
-----
| Mode  | Description                         |
|-------|-------------------------------------|
| 0     | From the current position to final  |
| 1     | In radius from the current position |

The command is simple:
```sh
./chunktool <posX> <posZ> <size> <mode>
```

Compile
-------
Just clone and make:
```
git clone https://gitlab.com/KanuX/ChunkTool.git ChunkTool
cd ChunkTool/
make
```

Contribution
------------
Are you sure that you want to contribute this?
