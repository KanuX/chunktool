#include "main.h"

#include "calculator.h"

Position player;

int main(int argc, char *argv[])
{
  u8 i, mode = 1;
  u32 size = 0;

  player.x.negative = 1;
  player.z.negative = 1;

  for(i=0; argv[i]; i++)
  {
    switch(i)
    {
      case 1:
      {
        player.x.pos = atof(argv[i]);
        if (player.x.pos < 0)
          player.x.negative = -1;
      }
      break;
      case 2:
      {
        player.z.pos = atof(argv[i]);
        if (player.z.pos < 0)
          player.z.negative = -1;
      }
      break;
      case 3: size = atoi(argv[i]); break;
      case 4: mode = atoi(argv[i]); break;
    }
  }

  chunkSize(player, size, mode);

  return 0;
}
