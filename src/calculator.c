#include "main.h"

void chunkSize(Position player, u32 size, u8 mode)
{
  i32 startX, startZ, finalX, finalZ;

  Chunk chunk;
  chunk.size = 16;
  chunk.start.x = (player.x.pos/chunk.size)*player.x.negative;
  chunk.start.z = (player.z.pos/chunk.size)*player.z.negative;

  switch(mode)
  {
    case 0:
    {
      chunk.final.x = chunk.start.x+size;
      chunk.final.z = chunk.start.z+size;
    } break;
    case 1:
    {
      chunk.final.x = chunk.start.x+size+1;
      chunk.final.z = chunk.start.z+size+1;
      chunk.start.x -= size;
      chunk.start.z -= size;
    }
    break;
  }

  startX = chunk.start.x*chunk.size;
  startZ = chunk.start.z*chunk.size;
  finalX = chunk.final.x*chunk.size;
  finalZ = chunk.final.z*chunk.size;

  printf("%d, %d - %d, %d\n", startX, startZ, finalX, finalZ);
}
